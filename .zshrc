## SETTINGS FROM MANJARO
## Options section
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.
setopt complete_aliases                                         # fixes git expansion
setopt prompt_subst                                             # enable substitution for prompt
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
HISTFILE=~/.zhistory
HISTSIZE=1000
SAVEHIST=500
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word

# autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'


# theming section  
autoload -U compinit colors zcalc
autoload -Uz vcs_info
compinit -d
colors

# left prompt
PROMPT="
%B%{%F{8}%}%(4~|%-1~/.../%2~|%~)%b%f 
%(?.%{%F{14}%}.%{%F{9}%})%(!.#.λ)%f "

# right prompt
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true                     # default is false - needed for icons
zstyle ':vcs_info:*' unstagedstr '%F{8} ●%f'                    # need the %u flag in formats to make this visible
zstyle ':vcs_info:*' stagedstr '%F{8} ●%f'                      # need the %c flag in formats to make this visible
zstyle ':vcs_info:git:*' formats '%F{8} %b%f%c'
zstyle ':vcs_info:git:*' actionformats '%F{1} %b%f'
RPROMPT='${vcs_info_msg_0_}'

# color man pages when using less
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

# set nvim as manpager
export MANPAGER="nvim +Man!"


## plugins section: enable fish style features
# use syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
# bind UP and DOWN arrow keys to history substring search
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up			
bindkey '^[[B' history-substring-search-down


# alias for committing dotfiles
alias ht="/usr/bin/git --git-dir=$HOME/.htdotfiles.git/ --work-tree=$HOME"
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

# vim keybindings
bindkey -v

# use .config/dircolors
test -r $XDG_CONFIG_HOME/dircolors && eval $(dircolors -b $XDG_CONFIG_HOME/dircolors)

# share history between terminals
setopt share_history

## alias section 
source ~/.config/htsetup/htalias

# keybindings for fzf
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# make systemd aware of environment variables
# place this at the end of the .zshrc
# check with systemctl --user show-environment
systemctl --user import-environment PATH

# nnn cd on exit
# -------------------- this block -------------------------------------------------
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn -e "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# run htpassmenu if it exists to prevent clipmenud tracking passwords
# if it does not exist then the default passmenu is run but clipmenud will track passwords
passmenu () {
    if command -v htpassmenu &>/dev/null; then
        htpassmenu
    else
        echo "htpassmenu not found - running default passmenu"
        command passmenu
    fi
}

# initialise zoxide for cd alternative
eval "$(zoxide init zsh)"

# set $PYTHONPATH environment variable
export PYTHONPATH="/usr/bin/python"

# pyenv setup
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# setup pdm to use pep582
if [ -n "$PYTHONPATH" ]; then
    export PYTHONPATH='/home/ht/.local/share/pdm/venv/lib/python3.10/site-packages/pdm/pep582':$PYTHONPATH
else
    export PYTHONPATH='/home/ht/.local/share/pdm/venv/lib/python3.10/site-packages/pdm/pep582'
fi
