# dotfiles
## basic setup
rename / move / delete any files that have the same name as those that are in this repository

clone the repository into the home directory (since the working tree is $HOME)
```sh
git clone --bare https://gitlab.com/mismatch/dotfiles.git $HOME/.dotfiles.git
```

checkout the new repository (there will be conflicts if any of these files already exist)
```sh
dotfiles checkout
```

only show tracked files (rather than all files in the home directory)
```sh
dotfiles config --local status.showUntrackedFiles no
```
## alias
this alias should already be in the repo

create an alias for using the dotfiles
```sh
echo 'alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"' >> $HOME/.zshrc
```
```sh
source ~/.zshrc
```

## gitignore
this is included in the repo

ignore all of the contents of the .dotfiles.git directory to avoid errors
```sh
echo ".dotfiles.git" >> $HOME/.gitignore
```

## use
use the alias `dotfiles` in the same way as normal `git` commands
```sh
dotfiles add -u
```
will add tracked files
